build:
	bundle exec jekyll build

serve:
	bundle exec jekyll serve --livereload --incremental

deploy: clean build test
	node ./node_modules/.bin/firebase deploy

init:
	npm install
	bundle install

clean:
	bundle exec jekyll clean

test: clean build image-check TODO-check

image-check:
	ruby image-check.rb

TODO-check: build
	! grep TODO -r _site
