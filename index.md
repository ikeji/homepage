---
layout: index
title: Shun Ikejima
docid: 681ac15a6a684381da74c4b621707024
---

Shun Ikejima
============

Contact
-------
* [email](mailto:syun@ikejima.org)
* [ostatus](https://ostatus.ikeji.ma/users/ikeji)
* LINE: [ikejixx](https://line.me/ti/p/Nm0FLRdNuy)
* WeChat: ikejix
* [Forms](https://forms.gle/kX9ewbm1LvkAuKvW9) (匿名で連絡したい時)

Article
-------
* [blog](http://blog.ikejima.org/ "ikejiの日記")
* [documents](http://docs.ikeji.ma/ "メモ")

Accounts
--------
* [Github](http://github.com/ikeji)
* [twitter](https://twitter.com/ikeji)
* [Facebook](https://www.facebook.com/shun.ikejima)
* [Tumblr](http://ikeji.tumblr.com/)
* [Instagram](https://www.instagram.com/ikejix/)
* [Linkedin](https://www.linkedin.com/pub/syun-ikejima/4/468/b29)
* [mixi](http://mixi.jp/show_profile.pl?id=32054)
* [Steam](http://steamcommunity.com/id/ikeji)
* [MyAnimeList](https://myanimelist.net/animelist/ikeji)

Sites
-----
* [Smile calender](http://smile.app.ikeji.ma/)
