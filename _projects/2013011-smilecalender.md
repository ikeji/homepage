---
title: Smile Calender
additional_images: [ webui.webp ]
tags: [ 2013, Web, React, JavaScript, Ruby ]
docid: 626efd25023b68a8737f05a224b5b42c
---
[smilecalendar.ikeji.ma](http://smilecalendar.ikeji.ma/)

どこかで見た、毎日笑顔を書きこむ式のカレンダーを自動生成するページを作ってみた。

運動した、喧嘩しなかった、笑顔で話せた、など決めた事を達成したら、
笑顔を書き込んで使う。

最初のバージョンはRubyで直接PDFを生成してた。

今のバージョンはReactとjspdfっていうライブラリで作ってある。
