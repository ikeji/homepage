---
title: The 'Egg' laptop
tags: [ 2022, 3D-print, RaspberryPi, keyboard, PC, pick ]
docid: b30f8272fb6dad0eadb93f84831d4de9
additional_images:
  - boot.webp
  - size.webp
  - inside.webp
used_images:
  - rotate.gif
  - cad.gif
---

Create a laptop.
ノートパソコンを作りたかった。

<!--more-->

## Backgorund

<img src="/projects/assets/2022091-egg-laptop-rotate.gif">

After I built [the cube PC](/projects/2021111-portablecubepc.html) , I want to build laptop PC as next project.

When I was kids, I draw my perfect laptop PC to note.
At that time, I couldn't have the laptop.
But, now I can start my project!

As a first step, I decided to create a orthodox clamshell style PC.
Which have a keyboard and screen.

I define a goal that I can design next laptop in this laptop.
On the other words, It only need to edit python code for cadQuery.

## Spec

- Raspberry Pi Zero W
- 7 inch LCD display
- 48Key ortho-linear keyboard.
- Size: WxDxH 198x190x34

## Design

<img src="/projects/assets/2022091-egg-laptop-cad.gif">

As usual, I design this laptop by [cadQuery](https://github.com/CadQuery/cadquery).

I checked other example in [Thingiverse](https://www.thingiverse.com/search?q=laptop+raspberrypi&page=1&type=things&sort=relevant).
But most other laptops are thicker than I expected.

But I found [GRIZ laptop](https://imgur.com/a/BkcIFII).
This one doesn't put CPU on back side of keyboard and display.
And achieve very thin laptop.

So I decided to use this style.

## Keyboard

Same as previous build, I use Cherry MX keyswitch.

I need 12x4 keys for comfortable typing.

To reduce the size of laptop, the keyboard have 16mm key pitch.

## Software

My laptop should be ready to use very quickly.

But RaspberryPi doesn't support suspend, and doesn't boot so quickly.

To achieve the goal, I wrote some code in bare-metal.

Now it's boot less than 5 sec.
I think this is limitation of bootloader for RaspberryPi.

But the software is not complete yet..

## Discussion / Quotes

- [Comments in Hackernews](https://news.ycombinator.com/item?id=33045219)
- [Comments in r/cyberdeck](https://www.reddit.com/r/cyberDeck/comments/xqigil/a_laptop_style_cyberdeck/)
- [An article in Hackster.io](https://www.hackster.io/news/shun-ikejima-s-egg-is-a-3d-printed-ultra-compact-ortholinear-raspberry-pi-zero-w-laptop-31f71ec3c36e)
- [An article in kbd.news](https://kbd.news/Egg-laptop-1668.html)

---

## 背景

<img src="/projects/assets/2022091-egg-laptop-rotate.gif">

[Cube型PC](/projects/2021111-portablecubepc.html)を作ったので、次はノートPCを作りたくなった。

子供の頃に、妄想してノートに想像図を書いてたノートPCそのものは作れなかったけど、
今なら作りはじめられるかも。

最初の一歩なので、
オーソドックスなクラムシェル型にしてキーボードと画面がある形式なのは決まった。

また、性能は、このノートPC自体を設計できるレベルにする事にした。
言いかえれば、エディタでソースコードが編集できればいい。

## スペック

- Raspberry Pi Zero W
- 7 インチ液晶
- 48キー オーソリニアキーボード
- Size: WxDxH 198x190x34

## デザイン

<img src="/projects/assets/2022091-egg-laptop-cad.gif">

今回も[cadQuery](https://github.com/CadQuery/cadquery)で設計をした。

[Thingiverse](https://www.thingiverse.com/search?q=laptop+raspberrypi&page=1&type=things&sort=relevant)などで既存の作例を見ると、厚さが厚いのが多くかっこよくない。

そんな中、[GRIZ](https://imgur.com/a/BkcIFII)というプロジェクトを発見した。
これは、キーボードの下にも画面の裏にも何も置かない事で、薄く仕上げている。

これを真似した配置にする事にした。

## キーボード

いつも通り、CherryMX互換キーを使って作った。

個人的に実用最低限だと思っている、12x4のキー数を配置した。

サイズが小さくなるように16mmキーピッチにしたが、
裏面のハリをがっつりつけたので剛性は確保できている。

## ソフトウエア

ノートパソコンなので、さっと取り出して使いたい。

しかし、RaspberryPiはサスペンドはできないし、
起動もかなり遅い。

そこで、ベアメタルプログラミングをし、
起動時間を稼ぐ事を考えた。

今の所書いたプログラムが画面を制御できるまで5秒ぐらいかかっている。
RaspberryPiのブートローダはu-bootのものに比べて遅いようだ。

今だ十分な機能が用意できていないので発継続中だ。

## 議論/評判

- [Hackernewsのスレ](https://news.ycombinator.com/item?id=33045219)
- [r/cyberdeckのスレ](https://www.reddit.com/r/cyberDeck/comments/xqigil/a_laptop_style_cyberdeck/)
- [Hackster.ioの記事](https://www.hackster.io/news/shun-ikejima-s-egg-is-a-3d-printed-ultra-compact-ortholinear-raspberry-pi-zero-w-laptop-31f71ec3c36e)
- [kbd.newsの記事](https://kbd.news/Egg-laptop-1668.html)
