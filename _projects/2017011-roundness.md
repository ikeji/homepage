---
title: 丸みチェッカ
additional_images: [ 1.webp, 2.webp, 3.webp ]
tags: [ 2017, 3D-print, sample ]
docid: 8ee431f24d29ffee4e9909a568d932d8
---
俺が使った物では角が丸い物体を作る事が多い。
しかし、どんぐらい角をまるくするのがいいか決めるのが難しい事が多い。
そこで、物の丸みを調べたり、自身がサンプルになってる物を作った。
