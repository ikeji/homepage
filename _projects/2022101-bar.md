---
title: "Gboard bar version"
tags: [ 2022, 3D-print, keyboard, HCI, AVR, M5, PCB ]
docid: 20f8ad717e9f905024e199680df05e54
additional_images:
---

We created longer keyboard.

長いキーボードを作った。

<!--more-->

## Circuit

At beginning, Using keyboard matrix is not good idea for this shape.

Using GPIO expander is one option, but we just use a set of shift registers.

To scan entire keyboard, we shift 136 times.

I heard that communicate for 2M by i2c is somewhat hard,
but this keyboard is worked without any problem.
(May because we use push-pull)

In the theory, we can extend this keyboard to infinite length,
but we didn't test and we don't know how long we can extend.

## Keycap

If we use per row tilt keycap like OEM profile,
You can see the difference between next keys.
To resolve this problem, I choose MDA profile.

We consider use ISO enter as eye catch, but we didn't select it this time.

Two keys (+ and enter) in numpad is rotated 90 digrees.
Do you notice this?

## Keymap

The keymap have multiple points to consider.

We put space bar at center and We image the user will sit here.

We put "ASDF" and "JKL;" to left and right for home position.

We assumed the user uses "vi" and put ESC key at most left position.
The user will move left when they hit ESC.

Hitting enter key quickly is important.
I put numpad enter to most right position.

## Body

When I created trial version, the strength of the keyboard is problem.
Because it's too long.

Later my team mate use aluminum frame for strength and the problem is resolved.

## 回路

まず最初に、形状的にマトリクスを組むのは難しそうだと思った。

GPIOエクスパンダーとi2cで通信するとかも考えられるけど、
ここはシンプルにシフトレジスタを並べる事にした。

つまり、キーボード全体をスキャンするためには、136回シフトしてる。

2メートル先のICに信号を送るのがどのぐらい難しいのかわからず、ビビっていたが、
やってみたら問題なく動作した。
i2cでは苦労するという話を聞く、プッシュプルだからうまくできたのかな(？)

シフトレジスタなので、論理的には無限に伸ばせるはずだが、
どこまで長くできるのかは試していない。

## キーキャップ

キートップの傾きが一定でないプロファイルを使ってしまうと、
横並びのキー同士で傾きが違ってガタガタしそうなので、
MDAプロファイルの物を選んだ。

見栄えを重視してISOエンターを入れるべきかどうかは悩んだが、入れなかった。

テンキーのプラスとエンターは90度回転してつけてある事に皆様お気づきになられただろうか？

## キーマップ

これもいろいろ悩んだ。

真ん中にスペースを置いて、
そこに座って使う想定でキーマップを作った。

左右に"ASDF"と"JKL;"を配置しホームポジションとした。

ユーザーはviを使っていると仮定して、
運動になるように左端の一番遠い所にESCを配置した。

右端にEnterを配置して、ターンって押せるようにしたがったが、
おさまりが悪かったので、右中程になった。
ただ、テンキーのEnterを右端に置いたので、これをターンできる。

## 筐体

基板のみの試作段階で、長いため筐体の強度が問題になりそうだと考えた。

最終的には同僚がアルミフレーム入りの筐体を設計してくれたので、
端を持って持ち上げられる(力があれば)ようになった。

