---
title: '3mk2023 (3D Mini Keyboard 2023)'
additional_images:
- right.webp
tags: [ 2022, 3D-print, keyboard, HCI, STM32 ]
docid: 4a6f53cf450deb2f4f54d0488b853c9e
---

Yet another variant of [3mk](/projects/2021091-3mk.html) series.
今年のバージョンの[3mk](/projects/2021091-3mk.html)を作った。

<!--more-->

[3mk](/projects/2021091-3mk.html) and [3mk2022](/projects/2022092-3mk2022.html) is my current daily driver.
(3mk is for home, 3mk2022 is for office)
But I had a trouble for some key of 3mk, so I decided to create next one.

The main difference is this one uses gateron low profile keys.

Gateron LP has mount tip for top plate on side instead of top and bottom,
To support this key, I redesign top place parts.

This time I use linear and low profile key, so it took some time to learn how to type this.

---

[3mk](/projects/2021091-3mk.html) と [3mk2022](/projects/2022092-3mk2022.html) の両方をメインのキーボードとして使ってる。
(3mkを家で、3mk2022を会社で。)

3mkのキーがいくつか効かなくなってしまった。
そこで、新しい3mkを作る事にした。

今年の目玉は、ロープロファイルのキー(gateron low profile)を使った事だ。

gateron lpは、プレートマウント用のツメの位置が90度違う。
そのために、キーをマウントしてる部分を設計しなおした。

ロープロファイルでリニア軸を使っているため、
フィードバッグが少ない、慣れるまで多少かかりそうだ。
