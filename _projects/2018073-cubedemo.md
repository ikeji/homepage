---
title: 立方体
additional_images: [ ]
tags: [ 2018, laser-cutter, wood ]
docid: d5c7d3c35bc94c0ffa0285d534da02ad
---
立方体を作るサンプル。

JavaScriptで生成コードを書いたので、
引数に渡したサイズでTスロットまで自動作成可能。
