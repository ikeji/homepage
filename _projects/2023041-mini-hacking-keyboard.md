---
title: "Mini Hacking Keyboard (MHKB)"
tags: [ 2023, 3D-print, keyboard, HCI, RP2040, pick ]
docid: 873cd335d243068149bfcd37699b0055
additional_images:
  - compare-to-hhk.webp
  - backplate.webp
  - backplate2.webp
  - pcb.webp
---

80% version of Happy hacking keyboard: Mini Hacking keyboard (MHKB)

Happy Hacking keyboardを80%に縮小したキーボード: Mini Hacking keyboard (略称: MHKB)

<!--more-->

My current hypnosis is "19mm keypitch is too larget to me".

To proof this hypnosis, I need a keyboard that exactly same keymap but smaller key pitch.
Mini hacking keyboard is a Happy hacking keyboard style keyboard but 16mm key pitch.

This keyboard has 25cm width until HHKB has 29.5cm width.

## PCB

I created PCB for this keyboard.

I found a kicad plugin that place each component based on JSON.
I modify it to:

- Support latest kicad
- Use python dict instead of JSON to avoid dependency.

Here is a [source](https://github.com/ikeji/kicad_component_layout).

## Case

I use 3D Printer for case.
The outer case is larger than my printer's width.
I print it with two pieces.
I added slash lines to hide the gap bitween pices.

## Firmware

Raspberry Pi Pico as controller and use KMK firmware.

This is first time that I use KMK firmware.
Since HHKB doesn't have complex functionality.
It's easy to implement keymap.

## Keycap

16mm key pitch means I can't use existing keycap.
So I 3d print keycap too.

Each keycaps are printed as a head and a stem separately.

## Conclusion

After I use this keyboard few month,
I think this keyboard has enough size for me.


今自分は、19mmキーピッチは大きすぎるんじゃないかという予想を立てている。

この予想を検証するには、19mmのキーボードと同じキーマップで、キーピッチだけが小さいキーボードが欲しい。
そこで、今回作ったMiniHackingKeyboardはHappyHackingKeyboardのキーマップを持つキーボードを16mmキーマップに縮小したものだ。

元のHHKBは29.5cmの幅を持つが、このキーボードは横幅25cmしかない。

## PCB

今回はキーボード用のPCBを作った。

キー配置が面倒と思ったが、探した所、JSONを元にキーを配置してくれるKiCADプラグインがみつかったので、以下の改造をして使っている：

- KiCADの最新版で動作するように改造
- JSONライブラリの依存関係を解決できなかったので、pythonのdictを使うように改造

[ソースをここに](https://github.com/ikeji/kicad_component_layout)置いた。

## ケース

今回も3Dプリンタでケースを作った。

横幅が3Dプリンタで印刷できるサイズを超えていたので、
2つに分割し、印刷後にビズで止めると一体になるように設計した。

デザインの斜線を入れ、分割点を見にくいようにした。

## ファームウエア

今回はラズピコをコントローラにしたので、
KMK Firmwareを使った。

KMK Firmwareを使ったのは初めてだったが、
複雑な機能や、外部デバイスを繋いでいないので、
簡単に必要な設定をする事ができた。

## キーキャップ

16mm キーピッチのため、
既存のキーキャップを使う事ができないため、
キーキャップも3Dプリンタでの印刷とした。

キートップとステムを別々に印刷して、圧入している。

## まとめ

数ヶ月使ってみたが、
このサイズのキーボードは自分にとっては十分なサイズのようだ。

