---
title: '"Dobule" キーボード'
additional_images:
  - case.webp
  - inside.webp
tags: [ 2020, 3D-print, keyboard, HCI, Trackball, pick ]
docid: 574926991661e0880a7fb2aacfca8201
---
5th daily-driver DIY keyboard.
常用してる自作キーボード5つ目 

<!--more-->

Build keyboard sensor ADNS5050.

To make it symmetric, I put trackball to both side.
I named it as "Double" because it have two balls.

In the daily use, I use right trackball for moving pointer and left ball for scrolling.

I want to have 3d keyboard, but It's too difficult to me and skip this time.
(I tried [next time](/projects/2021091-3mk.html))

The keyboard itself designed like "Let's split". So it's easy to switch to me.

For the trackball, the position of sensor is not perfect.
I'll improve next time.

<iframe width="556" height="988" src="https://www.youtube.com/embed/I-NZNXsY1xw" title="Double trackball double cursor #short" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/PqNR_MXHuuA?si=yZO3rQZDe4oMsj_9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

トラックボール用のセンサーADNS5050が入手できたので、
それを組み込んだーボードを作った。

トラックボールを左右に1つずつ2つつけたので、
名前をDoubleと呼んでいる。

右のボールはカーソル移動。
左はスクロールにつかっている。

最初は、キーボード部分も立体的にしようと考えたけど、
思ったより複雑になりそうだったので、今回はパス。
([次回](/projects/2021091-3mk.html)チャレンジした)

キーボード部分に関しては、
いつものレップリなので、使いやすさに問題はなし。

肝心のトラックボールが、センサーの位置がイマイチで自然に使えないのが、残念。
次回はもっと別な形で挑戦したい。


