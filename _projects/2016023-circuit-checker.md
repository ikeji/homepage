---
title: 回路確認機
additional_images: [ ]
tags: [ 2016, Tool ]
docid: a246e7708a4e155b318ee8505715d081
---
配線ミス、半田付けミスを発見できるように、確認機器を作った。

[元ネタはこれ][1]。
[実装ログはこれ][2]。

出力が0.5V以下なので、ほとんどのトランジスタのVf以下であり、
回路だけをチェックできて嬉しい。

[1]: http://elm-chan.org/works/cch/report.html
[2]: https://blog.ikejima.org/make/2016/02/10/circuit-checker.html
