---
title: CPUメータ
additional_images: [ demo.gif, design.webp, inside.webp ]
tags: [ 2018, laser-cutter, arduino, HCI, PC, wood, pick ]
docid: de0522ee7d0bb9dc843dfc95c2ee5f0c
---
PCにつないでCPU使用率、メモリ使用率などを表示するメーター

<!--more-->

格安の電圧計を見つけたので作ってみた。

ケースはMDFをレーザーカッターで加工して作った。

メータは元は100V交流用であった。
内部ではメータ本体に抵抗が直列に接続してあったので、
抵抗値を小さい物に変更した。これで0Vから3.xVの間のメーターになった。

ArduinoのPWMで制御している、微調整はArduino側で行ってる。

PCとの接続はArduino標準のシリアル、
PC側のスクリプトが数字3つの組をデバイスに送信し続けている。

<iframe width="560" height="315" src="https://www.youtube.com/embed/sg2Qq_2Xmec" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
