---
title: 自作キーボードその4
additional_images: 
  - board.webp
tags: [ 2019, CNC, laser-cutter, AVR, HCI, keyboard ]
docid: 53a2d55676fadceb2dfe0f23f0e7f80c
---

常用してる自作キーボード4つ目 

<!--more-->

[meishiキーボード](/projects/2019051-meishi.html)がいい感じだったので、
同じ技術を使って常用にキーボードを作った。

今回はサイズ制限がないので、基板を大きくした。
メンテナンス性を考慮して、ISP端子をつけたが、
これは、左右の通信用端子も兼ねている。

キー数が増えたので、ピン数が足りなくなり、
マトリクスを組む事になった。

キーキャップのステムはアクリルにした。
これで壊れる確率が減ったと思う。

最下行はカスタマイズすると思ったので、
冗談みたいなキーをつけた。

Fn - Meta - Turbo - Dash - Super - Duper - Hyper - Ultra - Extreme - Lunatic - Magical - Zen

仕事用にしばらく使っていた。
