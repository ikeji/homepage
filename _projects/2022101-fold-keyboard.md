---
title: "Foldable keyboard: Pittheus"
tags: [ 2022, 3D-print, keyboard, HCI, STM32, pick ]
docid: 1a4807138b80996c69598666863fe26e
additional_images:
  - fold.gif
  - setup.gif
  - keymap.png
---

Mobile Foldable Keyboard: Pittheus

持ち歩ける折り畳めるキーボード: ピッテウス

<!--more-->

I created mobile keyboard [before](/projects/2021011-minikeyboard2.html).

This time I make it more compact by folding.

## Folding

![fold](/projects/assets/2022101-fold-keyboard-fold.gif)

The main feature of this keyboad is folding.

I design hinge too.
Because the hinge is the key of creating compact keyboard.

## Keymap

![keymap](/projects/assets/2022101-fold-keyboard-keymap.png)

The keymap is based on 4x6 per half,
but added 2 more keys around hinges.

Both left and right hands are tilted 20°.
I picked this 20° angle from [Atreus keyboard](https://shop.keyboard.io/collections/keyboardio-atreus/products/keyboardio-atreus).

Also the name Pittheus is came from Atreus too.

I use 16mm keypitch for more compact.

Here is a typing scene.

<iframe width="560" height="315" src="https://www.youtube.com/embed/brRwNliE2L4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Mobile

![setup](/projects/assets/2022101-fold-keyboard-setup.gif)

The USB cable is fixed to keyboard.
The cable is also used for lock when fold it.

## Future work

I think it's too thick.
I think I should use low profile switch instead of Cherry MX style switch.

---

[過去にも](/projects/2021011-minikeyboard2.html)小型モバイルキーボードは作った事がある。

今回は折り畳める形のキーボードを作った。

## 折り畳み

![fold](/projects/assets/2022101-fold-keyboard-fold.gif)

このキーボードの主な特徴は折り畳める事だ。

ヒンジから自作して設計した。

単に既存のキーボードにヒンジをつけただけだとその部分が飛び出してしまうので、
その点を工夫をした。

## キー配列

![keymap](/projects/assets/2022101-fold-keyboard-keymap.png)

片手4x6を基本に、ヒンジのまわりにキーを追加している。

横一直線ではなく、
[アトレウスキーボード](https://shop.keyboard.io/collections/keyboardio-atreus/products/keyboardio-atreus)を参考に、
左右を20°傾けたスタイルにした。

(名前もアトレウスからもらってPittheusにした)

キーピッチは16mmにしたのでかなりコンパクトにできた。

タイピング動画:

<iframe width="560" height="315" src="https://www.youtube.com/embed/brRwNliE2L4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 持ち歩き

![setup](/projects/assets/2022101-fold-keyboard-setup.gif)

USBケーブルは本体固定で、
裏に面ファスナーをつけて閉じた時の固定にも利用してる。

## 今後の改良

厚さが厚いのが今後の改良点。
ロープロファイルスイッチを使ったらいいのだろうか。
