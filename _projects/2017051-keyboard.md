---
title: 自作キーボード
additional_images: [ back1.webp, back2.webp ]
tags: [ 2017, 3D-print, AVR, HCI, keyboard ]
docid: 05192f44051e82c15e51bf8705afa52e
---
自作キーボード
4行6列の分割キーボード。

<!--more-->

周囲でキーボードを作るのが流行したので、
俺も作ってみた。

経緯は[Facebookの投稿][1]とか、[Blog][2]が詳しい。

はじめてだったので下手だが、良い経験になったし、まだ使ってる。

[1]: https://www.facebook.com/okapies/posts/1421822534558931
[2]: https://blog.ikejima.org/make/keyboard/2017/12/21/keyboard.html
