---
title: 電気操作
additional_images: [ demo.gif ]
tags: [ 2016, 3D-print, Arduino, ESP8266 ]
docid: 6fb427e2305a016cb42a15ed32e2b924
---
IoT機器から、部屋の電気をつけたり消したりしたいという要望は多いと思うが、
実際作るとなると、100Vが通る線を作らねばならないので敷居が高い。

壁のスイッチの操作が可能だと聞いて、
そこで、壁のスイッチをネットワーク経由で操作できるようにしてみた。

<!--more-->

<iframe width="560" height="315" src="https://www.youtube.com/embed/mldgjdutAMs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
