---
title: 'テンキーパット'
additional_images:
  - inside.webp
  - case.webp
  - wireing.webp
tags: [ 2021, 3D-print, keyboard, HCI, AVR ]
docid: 86d2ed640d716dcfc5f85810c89c074a
---
テンキー(16キー)

<!--more-->

普段使ってるキーボードには数字キーはない。
しかし、確定申告の時期は独立したテンキーが欲しくなるので、
作る事にした。

全体を3Dプリントで作り、キーキャップとケースの形をあわせてデザインしてある。

無駄に二乗マトリクスで構成してある。

キーの刻印はレーザープリンター。
