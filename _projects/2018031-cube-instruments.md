---
title: 立方体楽器「プシュケ」
additional_images: [ usage.gif, inside.webp ]
tags: [ 2018, laser-cutter, STM32, Touch, Instruments, music ]
docid: a89c826847889ac60da78c7361973303
---
立方体型の楽器
24個のセンサーが各音階に対応してる。
<!--more-->
STM32のテストと、タッチセンサーのテストを兼ねて作った。

<iframe width="560" height="315" src="https://www.youtube.com/embed/QnOrQ68Wr8A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
