---
title: "Pyramid型キーボード"
tags: [ 2023, PCB, keyboard, HCI, STM32 ]
docid: 43533c92122b9654bdfd2c40894b7983
additional_images:
- pcb.webp
- pcb-details.webp
- pcb-setup.webp
- pcb-soldering.webp
- fingermove.png
- tilt.png
---

Build shape of the keyboard by PCB.

PCBでキーボードの形状を作るというコンセプトでキーボードを作った。

<!--more-->

I have 4 design goals.

## 3D PCB keyboard

My previous 3d keyboards are all hand wired keyboards.
For example, [3mk](/projects/2021091-3mk.html) is a hand wired keyboard.

I can hand wired these keyboard,
but it's work for one off keyboard.
I want to use PCB if I create many keyboards with same design.

Existing PCB-based keyboards I created are put keys on a same one plane.
One exception is [yunomi keyboard](/projects/2021101-yunomi.html). 
The yunomi keyboard uses 13 PCBs and puts keys on 3D position.
Each PCB was connected by wire.

In this pyramid keyboard, I solder PCB to PCB directory to build a 3d keyboard.

We still have big room to improve but I proof the idea is feasible.

## 90 digree yo rotate thumb cluster

When I open-close the hand, The axis for thumb finger is different from other fingers.

![fingermove](/projects/assets/2023111-pyramid-keyboard-fingermove.png)

If all keys are in same plane, it's hurts my thumb finger.
Then I want to put the thumb cluster to different plane.

## Reverse tilt

Usually keyboards are tilt to front.
But to save your wrist, it should tilt to back.

![tilt](/projects/assets/2023111-pyramid-keyboard-tilt.png)

Some ergonomic keyboards do this reverse tilt.

## Touch panel

Previously, I tried 
[Trackball](/projects/2020021-double-keyboard.html) or 
[Joystick](/projects/2022092-3mk2022.html) for pointing device in keyboard.

This time I'm trying touch panel screen.

It should work as touch pad.

---

4つの目標がある。

## 3D PCBキーボード

過去のキーボードで3D形状を持つものは全て手配線で作ってきた。、
例えば [3mk](/projects/2021091-3mk.html) は手配線である。

1台を手配線するのは簡単だが、何台も同じようなキーボードを手配線するのは飽きてくる。

一方、既存のキーボードは平面上にキーを配置している事が多かった。

自分の作った物で例外は、[湯呑み型キーボード](/projects/2021101-yunomi.html)だ。
このキーボードでは、13枚の基板を立体的に配置してキーボードを作っている。
この時はジャンパで基板間を繋いでいた。

この考え方を押し進めて、PCBをPCBに半田付けする事で、立体的なキーボードを作れないかと考えたのが、
このキーボードだ。

まだまだチューニングの余地があるが、
PCBで立体物を作れる事は証明できたと思う。

## 親指の方向を90度変える

手を空中で開いたり閉じたりするとわかるが、
親指とその他の指では動く向きが90度違う。

![fingermove](/projects/assets/2023111-pyramid-keyboard-fingermove.png)

普通のキーボードのように親指用のキーを同じ平面に置くと、
親指の横でキーを打つ事になり、
指が痛くなる。

そこで親指とその他のキーを別の平面に置いた。

## 逆チルト

一般的にキーボードは自分の方向を向けて傾ける場合が多い、
しかし、手首まっすぐにしたいと考えると、キーボードは向こう側に向けるべきなのではないかと思われる。

![tilt](/projects/assets/2023111-pyramid-keyboard-tilt.png)

いくつかのエルゴノミックキーボードはこれを採用している。

今回は大胆にメインのキー群を前にむけた。

## タッチパネルを配置

キーボードに内蔵するポインティングデバイスとして、
[トラックボール](/projects/2020021-double-keyboard.html)や
[アナログジョイステック](/projects/2022092-3mk2022.html)を試してきたが、
今回は、タッチパネルつきの画面を用意した。
親指でタッチパットのように使用できる。

