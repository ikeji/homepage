---
title: "Writing ball keyboard"
tags: [ 2023, 3D-print, keyboard, HCI, STM32 ]
docid: c8546eeb63c858dc3b3c7df702c455bf
additional_images:
- type.webp
- zoom.webp
- keycap.webp
used_images:
- wikipediaimg.webp
- wikipediaimg2.webp
---

Ball style keyboard

半球体キーボードを作った。

<!--more-->

## Typing style

There are two major typing style: Piano style and nade style.

Piano style

- Use tip of the finger to type.
- Mainly move joint of finger to select key.
- We can do more precise control.

Nade style


- Use finger pad to type.
- Move finger and type in same time.
- More large contact area -> Less tired.

I mainly use piano-style typing and design keyboard for piano-style.

This time I'm focus to nade style typing.

## Hansen writing ball

[Hansen writing ball](https://en.wikipedia.org/wiki/Hansen_Writing_Ball) is one of the first typewriter.

![](/projects/assets/2023091-writing-ball-wikipediaimg.webp)

Image from [wikipedia](https://commons.wikimedia.org/wiki/File:Schreibkugel_von_Hans_Rasmus_Johann_Malling_Hansen_02.jpg)

These days many elgonomic keyboard puts the keybs inside sphere.
Include my 3mk keyboard.

But the hansen writing ball puts keys outside of sphere.

I think it's good for nade style typing.

## iMac G4 Style

The spherical machine reminds me of the iMac G4.

![](/projects/assets/2023091-writing-ball-wikipediaimg2.webp)

Image from [wikipedia](https://commons.wikimedia.org/wiki/File:IMac_G4_sunflower8.png)

iMac G4 has display arm to the spherical machine.
I want to use this idea and design display arm for this keyboard.

## タイピングスタイル

ピアノ打ちと、なで打ちという、2つの有名なタイピングスタイルがある。

ピアノ打ち

- キーを指先で打つ
- 指の関節を使って打つキーまで指を移動する。
- 指を細かく操作する事ができる。

なで打ち

- 指の腹でキーを打つ。
- 指の移動とタイピングを同時に行なう。
- キートップに触る面積が大きい -> 疲れにくい。

私は普段はピアノ打ちをしているし、それに合うキーボードを設計してきた。

今回はなで打ちに特化したキーボードを作ろうと思う。

## ハンセンのライティングボール

[ハンセンのライティングボール](https://en.wikipedia.org/wiki/Hansen_Writing_Ball)は初期のタイプライターだ。

![](/projects/assets/2023091-writing-ball-wikipediaimg.webp)

Image from [wikipedia](https://commons.wikimedia.org/wiki/File:Schreibkugel_von_Hans_Rasmus_Johann_Malling_Hansen_02.jpg)

今日、エルゴノミックキーボードは、
キーを球体の内側に沿って配置する事が多い。
私の3mkもそうなっている。

ハンセンのライティングボールは逆に球体の外側にキーを配置している。

これはなで打ちに合っているのではないかと考えた。

## iMac G4 Style

球体形のマシンと言って思い出されるのは、鏡餅形iMacだ。

![](/projects/assets/2023091-writing-ball-wikipediaimg2.webp)

Image from [wikipedia](https://commons.wikimedia.org/wiki/File:IMac_G4_sunflower8.png)

鏡餅形iMacは、半球形の本体からディスプレーアームが伸びている形をしている。
今回もそれを真似したくなったので、アームとディスプレイをつけた。

