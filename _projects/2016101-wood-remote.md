---
title: リモコン
additional_images: [ 2.webp, back.webp, board.webp, board-back.webp, button.webp, case.webp ]
tags: [ 2016, wood, IR, AVR ]
docid: e0b2af71ca528dcb45f1a854ff3f1a60
---
テレビを見ながら何となく触る事も多いリモコンを、
プラスチックではなくて、木で作ってみた。

ケースは2x4材をカットしてくりぬいて作った。

中身はいつものAVR。
基板は銅テープ。
