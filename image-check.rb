#!/usr/bin/env ruby

require 'yaml'

PROJECT_FILES = Dir['_projects/*.md']
PROJECTS = (PROJECT_FILES.map do |proj|
  [proj, proj.match(/_projects\/(.*)\.md/)[1]]
end)
USED_IMAGES = []
IMAGES = Dir['_projects/assets/*']

IMAGES.each do |img|
  raise "image should be webp,png or gif" unless
    img.match(/\.(gif|webp|png|mp4)$/)
end

# Check all projects has main image.
PROJECTS.each do |proj|
  img = "_projects/assets/#{proj[1]}.webp"
  raise "No main image #{img}" if
      not IMAGES.include? img 
  USED_IMAGES.push img
  thumb = "_projects/assets/#{proj[1]}.thumb.webp"
  raise "No tumb image #{thumb}" if
      not IMAGES.include? thumb
  USED_IMAGES.push thumb
end

# Check all project's subimages are exists.
PROJECTS.each do |proj|
  content = File.read proj[0]
  yaml = content.match(/\A(---.*?)---/m)
  next if not yaml
  obj = YAML.load(yaml[1])
  images = (obj["additional_images"]||[]) + (obj["used_images"]||[])
  next if not images
  images.each do |im|
    next if im == ''
    img = "_projects/assets/#{proj[1]}-#{im}"
    raise "No sub image #{img}" if
        not IMAGES.include? img 
    USED_IMAGES.push img
  end
end

# Check all images has a project.
UNUSED_IMAGES = IMAGES - USED_IMAGES
raise "Unused #{UNUSED_IMAGES.size} images: #{UNUSED_IMAGES.sort}" if UNUSED_IMAGES.size > 0
